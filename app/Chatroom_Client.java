package Exercise3;

import java.awt.Button;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Panel;
import java.awt.TextArea;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.Socket;

import javax.swing.JFrame;

@SuppressWarnings("serial")
public class Chatroom_Client extends JFrame implements ActionListener,Runnable {

	Socket socket;
	TextField tf;
	TextArea result;
	Button send_btn;
	Panel pn,pn1,pn2;
	
	boolean clientOn = true;
	
	String CLOSE_CODE = "close";
	String OPEN_CODE = "open";
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new Chatroom_Client();
	}
	
	public Chatroom_Client() {
		GUI();
		this.setDefaultCloseOperation(3);
		try{
			this.socket = new Socket("localhost",3000);
			DataOutputStream dos = new DataOutputStream(socket.getOutputStream());
			dos.writeUTF(OPEN_CODE);
		}catch(Exception e) {
			
		}
		Thread t = new Thread(this);
		t.start();
		this.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e){
				try {
					DataOutputStream dos = new DataOutputStream(socket.getOutputStream());
					dos.writeUTF(CLOSE_CODE);
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
	}

	@SuppressWarnings("deprecation")
	private void GUI() {
		// TODO Auto-generated method stub
		pn1=new Panel(new GridLayout(1,1));
		pn2=new Panel(new FlowLayout());
		pn=new Panel(new FlowLayout());		
		tf = new TextField();
		tf.setPreferredSize(new Dimension(390,25));
		result = new TextArea();
		result.disable();
		send_btn = new Button("Send");
		send_btn.addActionListener(this);
		
		pn2.add(tf);
		pn2.add(send_btn);
		pn1.add(result);
		pn.add(pn1);
		pn.add(pn2);
		this.add(pn);
		this.setSize(500,300);	
		this.setVisible(true);
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		while(clientOn) {
			try {
				DataInputStream dis = new DataInputStream(socket.getInputStream());
				String reply = dis.readUTF();
				if(reply.equals(CLOSE_CODE)) {
					socket.close();
					clientOn = false;
				}
				else {
					result.append(reply+"\n");
				}
			}catch(Exception e) {
				
			}
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if(e.getSource()==send_btn) {
			String s = tf.getText();
			try{
				DataOutputStream dos = new DataOutputStream(socket.getOutputStream());
				dos.writeUTF(s+"");
				result.append("You: "+s+"\n");
			}catch(Exception ex) {
				
			}
		}
	}
}
